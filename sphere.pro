#-------------------------------------------------
#
# Project created by QtCreator 2014-08-19T15:30:01
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = sphere
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    drawsphere.cpp \
    spherepanel.cpp

HEADERS  += mainwindow.h \
    drawsphere.h \
    spherepanel.h

FORMS    += mainwindow.ui
