#ifndef SPHEREPANEL_H
#define SPHEREPANEL_H

#include <QWidget>
#include <QPaintEvent>
#include <QList>
#include "drawsphere.h"
class SpherePanel : public QWidget
{
    Q_OBJECT
public:
    explicit SpherePanel(QWidget *parent = 0);
    ~SpherePanel();
    void addHar(double rot_speed,int length);
    void delete_last_bar();
protected:
    void paintEvent(QPaintEvent*);
    QPointF _draw_Line(QPainter&,int y);
private:
    SphereManager spheremanager;
    QList<SphereManager*>* list;
signals:

public slots:
};

#endif // SPHEREPANEL_H
