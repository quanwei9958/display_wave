#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QGridLayout>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->resize(600,500);

    this->panel=new SpherePanel(this);
    this->panel->setGeometry(0,0,500,500);

    this->leftlayout=new QGridLayout();
    this->rightlayout=new QVBoxLayout();
    this->mainlayout=new QHBoxLayout();
    this->leftlayout->addWidget(this->panel,0,0);

    this->input_xiebo=new QLineEdit(this);
    input_xiebo->setText("输入频率");
    this->input_xiebo->setGeometry(500,0,100,20);

    this->input_length=new QLineEdit(this);
    this->input_length->setText("输入幅值");
    this->input_length->setGeometry(500,25,100,20);

    this->add_button=new QPushButton(this);
    this->add_button->setText("确认添加");
    this->add_button->setGeometry(500,50,100,20);

    this->delete_button=new QPushButton(this);
    this->delete_button->setText("删除最后添加");
    this->delete_button->setGeometry(500,75,100,20);


    //this->rightlayout->addWidget(this->input_xiebo);
    //this->rightlayout->addWidget(this->add_button);


    //this->mainlayout->addLayout(this->leftlayout);
    //this->mainlayout->addLayout(this->rightlayout);


    //this->mainwidget=new QWidget();
    //this->mainwidget->setLayout(this->mainlayout);
    //this->setCentralWidget(this->mainwidget);


    this->timer=new QTimer();
    this->timer->setInterval(100);
    this->timer->start();
    connect(timer,SIGNAL(timeout()),this,SLOT(update()) );
    connect(this->add_button,SIGNAL(clicked()),this,SLOT(add_sphere()));
    connect(this->delete_button,SIGNAL(clicked()),this,SLOT(delete_sphere()));
}
#define PI 3.1415926
void MainWindow::add_sphere()
{
    bool allright=false;
    QString row=this->input_xiebo->text();
    QString length=this->input_length->text();
    double row_1=row.toDouble(&allright);
    if(!allright)
        return;
    double length_1=length.toDouble(&allright);
    if(!allright)
        return;

    //one step is 0.1s
    row_1=(2*PI*row_1)/3000;
    this->panel->addHar(row_1,length_1);
}
void MainWindow::delete_sphere()
{
    this->panel->delete_last_bar();
}
MainWindow::~MainWindow()
{
    delete ui;
    delete panel;
    delete mainlayout;
    delete timer;
    delete rightlayout;
    delete leftlayout;
    delete add_button;
    delete input_xiebo;
}
