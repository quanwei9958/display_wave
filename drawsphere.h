#ifndef DRAWSPHERE_H
#define DRAWSPHERE_H
#include <QPainter>
#include <QPoint>
#include <QColor>
class SphereManager
{
public:
    SphereManager();
    void drawSphere(QPainter& painter);
    void drawSphere(QPainter& painter,QPointF &center);
    void setCenter(QPointF& center);
    QPointF getCurrentPoint() const;
    QColor getColor() const;
    void setSpeed(double);
    void setR(double);
    void set_visible_sphere(bool y);
protected:
    QPointF center;
    QPointF lastpoint;
    QColor color;
    double R;
    double speed;
    double currentRow;
private:
    void draw_true_sphere(QPainter& painter,QPoint& center,int R);
    QPointF cal_draw_point();
    bool visible_sphere;
};

#endif // DRAWSPHERE_H
