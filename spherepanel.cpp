#include "spherepanel.h"
#include <malloc.h>
#include <QColor>
#include <QLine>
#include <QList>
#define MAX_POINT 240
SpherePanel::SpherePanel(QWidget *parent) :
    QWidget(parent)
{
    this->list=new QList<SphereManager*>();
    this->addHar(0.1,30);
    this->addHar(0.2,20);
    this->addHar(0.5,15);
}
SpherePanel::~SpherePanel()
{
    while(!list->isEmpty())
        delete list->takeFirst();
}
void SpherePanel::delete_last_bar()
{
    if(!this->list->empty())
        this->list->takeLast();
}
void SpherePanel::addHar(double rot_speed, int length)
{
    SphereManager *sphere=new SphereManager();
    sphere->setSpeed(rot_speed);
    sphere->setR(length);
    this->list->append(sphere);
}
void SpherePanel::paintEvent(QPaintEvent *)
{
    //color value rgb:222 123 102
    QPainter painter(this);
    QPointF p(this->width()/5,250);
    painter.setBackground(QBrush(QColor(222,123,102)));
    painter.fillRect(QRectF(0,0,this->width(),this->height()) , painter.background());
    int count=this->list->count();
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.beginNativePainting();
    for(int i=0;i<count;++i)
    {
        SphereManager* temp=this->list->at(i);
        painter.setPen(QPen(temp->getColor()));
        temp->set_visible_sphere(i==(count-1));
        temp->drawSphere(painter,p);
        p=temp->getCurrentPoint();
    }
    painter.endNativePainting();
    QPointF end_point=_draw_Line(painter,p.y());
    QPen pen(Qt::color0);
    pen.setWidth(1);
    painter.setPen(pen);
    painter.drawLine(p,end_point);
}
QPointF SpherePanel::_draw_Line(QPainter& painter,int y)
{
    static QList<int> list;
    const int step=1;
    list.append(y);

    const int count=list.count();
    const double offset=this->width()*2/5.0;
    QLineF line[count-1];
    for(int i=0;i<count-1;++i)
    {
        line[count-i-2].setLine(offset+i*step,list.at(i),offset+(i+1)*step,list.at(i+1));
    }
    QPen pen;
    pen.setColor(Qt::black);
    pen.setWidth(2);
    painter.setPen(pen);
    painter.drawLines(line,count-1);
    if(count>=MAX_POINT)
        list.takeFirst();
    return QPointF(step*count+offset,y);
}
