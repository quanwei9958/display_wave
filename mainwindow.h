#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "spherepanel.h"
#include <QGridLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QTimer>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void add_sphere();
    void delete_sphere();
private:
    Ui::MainWindow *ui;
protected:
    QTimer* timer;
    SpherePanel* panel;
    QGridLayout* leftlayout;
    QVBoxLayout* rightlayout;
    QHBoxLayout* mainlayout;
    QPushButton* add_button;
    QPushButton* delete_button;
    QLineEdit* input_xiebo;
    QLineEdit* input_length;
};

#endif // MAINWINDOW_H
