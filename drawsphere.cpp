#include "drawsphere.h"
#include <QPainter>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#define PI 3.1415926

SphereManager::SphereManager()
{
    this->visible_sphere=true;
    this->speed=0.2;
    this->currentRow=0;
    this->R=10;
    this->center=QPointF(0,0);
    this->color.setRed(rand()%255);
    this->color.setBlue(rand()%255);
    this->color.setGreen(rand()%255);
}
void SphereManager::setCenter(QPointF &center)
{
    this->center=center;
}
void SphereManager::setR(double a)
{
    this->R=a;
}
void SphereManager::set_visible_sphere(bool y)
{
    this->visible_sphere=y;
}
void SphereManager::setSpeed(double a)
{
    this->speed=a;
}
void SphereManager::draw_true_sphere(QPainter& painter,QPoint& center,int R)
{
    painter.drawEllipse(center,R,R);
}
QPointF SphereManager::getCurrentPoint() const
{
    return this->lastpoint;
}
QPointF SphereManager::cal_draw_point()
{
    return QPointF(R*cos(this->currentRow),R*sin(this->currentRow));
}
void SphereManager::drawSphere(QPainter &painter)
{
    QPointF one=this->cal_draw_point();
    painter.fillRect(QRectF(QPointF(one.x()-2.5,one.y()-2.5),QSize(5,5)),QBrush(Qt::red) );
}
QColor SphereManager::getColor() const
{
    return this->color;
}
void SphereManager::drawSphere(QPainter &painter, QPointF& center)
{
    this->setCenter(center);
    this->currentRow+=this->speed;
    painter.translate(center.x(),center.y());
    if(visible_sphere)
        this->drawSphere(painter);
    this->lastpoint=this->cal_draw_point()+center;
    painter.translate(-center.x(),-center.y());
    painter.drawLine(center,this->lastpoint);
    QPen pen;
    pen.setWidth(2);
    pen.setColor(painter.pen().color());
    painter.setPen(pen);
    painter.drawEllipse(center,this->R,this->R);
}
